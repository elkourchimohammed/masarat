# Stage 1: Compilation de l'application
FROM maven:3-jdk-8-alpine AS build

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

# Stage 2: Package de l'application
FROM openjdk:8-jdk-alpine 

WORKDIR /app

COPY --from=build /usr/src/app/target/helloworld-0.0.1.war .

ENV PORT 8080
EXPOSE $PORT

ENTRYPOINT ["java", "-jar", "helloworld-0.0.1.war"]

